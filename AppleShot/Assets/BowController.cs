﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowController : MonoBehaviour
{
    public GameObject ArrowPrefab;
    float spen = 0.5f;
    float delta = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(0, 0, 5);
        }

        if(Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(0, 0, -5);
        }

        this.delta += Time.deltaTime;
        if(delta > spen)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                this.delta = 0;
                GameObject arrow = Instantiate(ArrowPrefab) as GameObject;
                arrow.transform.position = transform.position;
                arrow.transform.rotation = transform.rotation;
                GameObject director = GameObject.Find("CountDirector");
                director.GetComponent<CountDirector>().IncreasCo();
            }
        }
    }
}
