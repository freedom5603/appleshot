﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDirector : MonoBehaviour
{
    public int count;
    public GameObject counter = null;
    // Start is called before the first frame update
    void Start()
    {
        this.counter = GameObject.Find("Counter");
    }

    public void IncreasCo()
    {
        count++;
    }

    // Update is called once per frame
    void Update()
    {
        Text arrowText = this.counter.GetComponent<Text>();
        arrowText.text = count + "本";
    }
}
