﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    Rigidbody2D rigid2D;
    float boundForce = 600.0f;
    GameObject ringo;


    // Start is called before the first frame update
    void Start()
    {
        this.rigid2D = GetComponent<Rigidbody2D>();
        this.rigid2D.AddForce(transform.up * this.boundForce);
    }

    // Update is called once per frame
    void Update()
    {
        // transform.LookAt(transform.position - prePos + transform.position);

        if (transform.position.y < -6)
        {
            Destroy(gameObject);
        }

       
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
        GameObject generator = GameObject.Find("RingoGenerator");
        generator.GetComponent<RingoGenerator>().Appear();
    }
}
