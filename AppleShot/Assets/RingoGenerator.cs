﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RingoGenerator : MonoBehaviour
{
    public GameObject RingoPrefab;
    int count = 0;
    // Start is called before the first frame update
    void Start()
    {
        GameObject ringo = Instantiate(RingoPrefab) as GameObject;
        int y = Random.Range(-4, 4);
        ringo.transform.position = new Vector3(6, y, 0);
    }

    // Update is called once per frame
    public void Appear()
    {
        if(count == 10)
        {
            SceneManager.LoadScene("ResultScene");
        }
        GameObject ringo = Instantiate(RingoPrefab) as GameObject;
        int y = Random.Range(-4, 4);
        ringo.transform.position = new Vector3(6, y, 0);
        count++;
    }
}
